import re
from typing import Dict, List, Optional

from pydantic import BaseModel, root_validator

import utils
import enum


class SeedMapEntryType(str, enum.Enum):
    source = "source"
    destination = "destination"


class SeedAnalysisEntry(BaseModel):
    source: int
    destination: int
    value: int
    offset: Optional[int] = None

    @root_validator(pre=True)
    def calculate_offset(cls, values) -> int:
        values["offset"] = values["destination"] - values["source"]

        return values

    def __lt__(self, other):
        return self.value < other.value


class SeedMapEntry(BaseModel):
    destination_start: int
    destination_end: int
    source_start: int
    source_end: int
    length: int
    offset: int


class SeedMap(BaseModel):
    source: str
    destination: str
    entries: List[SeedMapEntry] = []

    def transform_source(self, transform: int) -> int:
        for index, r in enumerate(self.entries):
            if transform >= r.destination_start and transform <= r.destination_end:
                return transform - r.offset

        return transform

    def transform_destination(self, transform: int) -> int:
        for index, r in enumerate(self.entries):
            if transform >= r.source_start and transform <= r.source_end:
                return transform + r.offset

        return transform

    def get_range_points(
        self, type: SeedMapEntryType, with_edges: bool = True
    ) -> List[int]:
        range_points: List[int] = []

        for e in self.entries:
            if type == SeedMapEntryType.source:
                e_range_points = [
                    e.source_start,
                    e.source_end,
                ]
                if with_edges:
                    e_range_points.extend(
                        [
                            e.source_start - 1,
                            e.source_start + 1,
                            e.source_end - 1,
                            e.source_end + 1,
                        ]
                    )

            if type == SeedMapEntryType.destination:
                e_range_points = [
                    e.destination_start,
                    e.destination_end,
                ]
                if with_edges:
                    e_range_points.extend(
                        [
                            e.destination_start - 1,
                            e.destination_start + 1,
                            e.destination_end - 1,
                            e.destination_end + 1,
                        ]
                    )

            if not e_range_points:
                raise Exception(f"Range points missing for entry: {e}")

            range_points.extend(e_range_points)

        return range_points

    def merge(self, map: "SeedMap") -> "SeedMap":
        sum_map = SeedMap(source=self.source, destination=map.destination)
        dest_range_points = self.get_range_points(type=SeedMapEntryType.destination)
        source_range_points = map.get_range_points(type=SeedMapEntryType.source)
        valid_points = sorted({p for p in (dest_range_points + source_range_points)})
        analysis_points: List[SeedAnalysisEntry] = []

        for p in valid_points:
            destination = map.transform_destination(p)
            source = self.transform_source(p)
            analysis_point = SeedAnalysisEntry(
                source=source, destination=destination, value=p
            )
            analysis_points.append(analysis_point)

        analysis_points = sorted(analysis_points)

        merged_ranges: List[SeedMapEntry] = []
        merged_range: Optional[SeedMapEntry] = None
        for p in analysis_points:
            if not p.offset:
                continue
            if merged_range and p.offset == merged_range.offset:
                merged_range.source_end = p.source
                merged_range.destination_end = p.destination
                merged_range.length = p.source - merged_range.source_start + 1
            else:
                if merged_range:
                    merged_ranges.append(merged_range.model_copy())

                merged_range = SeedMapEntry(
                    source_start=p.source,
                    source_end=p.source,
                    destination_start=p.destination,
                    destination_end=p.destination,
                    length=p.source - p.source + 1,
                    offset=p.offset,
                )

        sum_map.entries = merged_ranges
        return sum_map


def parse_input(input: str) -> (List[int], Dict[str, SeedMap]):
    input_lines = utils.multiline_str_to_list(multiline_str=input)
    line_regex = r"^([\w-]+):?\s+(map)?([\d\s]+)?"

    maps_data: Dict[str, SeedMap] = {}
    seeds: List[int] = []
    map_name: Optional[str] = None
    map_source: Optional[str] = None
    map_destination: Optional[str] = None
    for l in input_lines:
        results = re.findall(line_regex, l)
        regex_matches = results[0]

        if map_name is None:
            map_name = regex_matches[0]
            seeds = [int(s) for s in re.split("\s+", regex_matches[2])]
            continue

        if regex_matches[1] == "map":
            map_name = regex_matches[0]
            map_source, _, map_destination = map_name.split("-")
            maps_data[map_destination] = SeedMap(
                source=map_source, destination=map_destination
            )
            continue
        else:
            parsed_line = [int(i) for i in re.split("\s+", l)]
            if len(parsed_line) != 3:
                raise Exception(f"Unable to properly parse seeds map: {l}")

            source_start = parsed_line[1]
            destination_start = parsed_line[0]
            length = parsed_line[2]
            range_offset = destination_start - source_start
            map_entry = SeedMapEntry(
                destination_start=destination_start,
                destination_end=destination_start + length - 1,
                source_start=source_start,
                source_end=source_start + length - 1,
                length=length,
                offset=range_offset,
            )
            maps_data[map_destination].entries.append(map_entry)

    return seeds, maps_data


def get_seed_to_location_map(soil_maps: Dict[str, SeedMap]) -> SeedMap:
    return (
        soil_maps["soil"]
        .merge(soil_maps["fertilizer"])
        .merge(soil_maps["water"])
        .merge(soil_maps["light"])
        .merge(soil_maps["temperature"])
        .merge(soil_maps["humidity"])
        .merge(soil_maps["location"])
    )


def parse_puzzle1(input: str) -> int:
    seeds, soil_maps = parse_input(input=input)
    seed_to_location = get_seed_to_location_map(soil_maps=soil_maps)
    locations = [seed_to_location.transform_destination(s) for s in seeds]

    return min(locations)


def parse_puzzle2(input: str) -> int:
    seeds, soil_maps = parse_input(input=input)

    seed_to_location = get_seed_to_location_map(soil_maps=soil_maps)
    map_points = {
        p
        for p in seed_to_location.get_range_points(
            type=SeedMapEntryType.source, with_edges=False
        )
    }
    range_points: List[int] = []

    for seed_ranges in range(0, int(len(seeds) / 2)):
        seed_range_start = seeds[seed_ranges * 2]
        seed_range_end = seeds[seed_ranges * 2] + seeds[seed_ranges * 2 + 1] - 1
        range_points.extend([seed_range_start, seed_range_end])
        for map_point in map_points:
            if map_point > seed_range_start and map_point < seed_range_end:
                range_points.append(map_point)

    valid_points = {p for p in range_points}
    valid_locations = {
        seed_to_location.transform_destination(p)
        for p in valid_points
        if seed_to_location.transform_destination(p)
    }

    return min(valid_locations)
