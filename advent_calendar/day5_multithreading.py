import logging
import multiprocessing
import queue
import re
from typing import Dict, List, Optional

from pydantic import BaseModel

import utils


class TransformMap(BaseModel, arbitrary_types_allowed=True):
    source: str
    destination: str
    ranges: List[range] = []
    range_offset: List[int] = []

    def add_range(self, r: range, offset: int) -> None:
        self.ranges.append(r)
        self.range_offset.append(offset)

    def __call__(self, transform: int) -> int:
        for index, r in enumerate(self.ranges):
            if transform >= r.start and transform < r.stop:
                return transform + self.range_offset[index]

        return transform


class TransformRange(BaseModel, arbitrary_types_allowed=True):
    source: str
    ranges: List[range] = []

    def transform(self, map: TransformMap) -> "TransformRange":
        range_points: List[List[int]] = []
        transformed_range = TransformRange(source=map.destination)
        for index, sr in enumerate(self.ranges):
            range_points.append([sr.start, sr.stop])
            for tr in map.ranges:
                if tr.start > sr.start and tr.start < sr.stop:
                    new_start = tr.start
                    new_stop = new_start - 1
                    if new_start not in range_points[index]:
                        range_points[index].append(new_start)
                    if new_stop not in range_points[index]:
                        range_points[index].append(new_stop)
                if tr.stop > sr.start and tr.stop < sr.stop:
                    new_stop = tr.stop
                    new_start = new_stop + 1
                    if new_start not in range_points[index]:
                        range_points[index].append(new_start)
                    if new_stop not in range_points[index]:
                        range_points[index].append(new_stop)

        for points in range_points:
            reversed = False
            if points[0] > points[1]:
                reversed = True
            points.sort(reverse=reversed)
            for i in range(0, int(len(points) / 2)):
                r_start = map(points[i * 2])
                r_stop = map(points[i * 2 + 1])
                if r_start <= r_stop:
                    new_range = range(r_start, r_stop)
                else:
                    new_range = range(r_stop, r_start)
                transformed_range.ranges.append(new_range)

        return transformed_range

    def min_range_value(self) -> int:
        min_value: Optional[int] = None
        for r in self.ranges:
            if min_value is None or min_value > r.start:
                min_value = r.start
            if min_value is None or min_value > r.stop:
                min_value = r.stop

        return min_value


def parse_input(input: str) -> (List[int], Dict[str, TransformMap]):
    input_lines = utils.multiline_str_to_list(multiline_str=input)
    line_regex = r"^([\w-]+):?\s+(map)?([\d\s]+)?"

    maps_data: Dict[str, TransformMap] = {}
    seeds: List[int] = []
    map_name: Optional[str] = None
    map_source: Optional[str] = None
    map_destination: Optional[str] = None
    for l in input_lines:
        results = re.findall(line_regex, l)
        regex_matches = results[0]

        if map_name is None:
            map_name = regex_matches[0]
            seeds = [int(s) for s in re.split("\s+", regex_matches[2])]
            continue

        if regex_matches[1] == "map":
            map_name = regex_matches[0]
            map_source, _, map_destination = map_name.split("-")
            maps_data[map_destination] = TransformMap(
                source=map_source, destination=map_destination
            )
            continue
        else:
            parsed_line = [int(i) for i in re.split("\s+", l)]
            if len(parsed_line) != 3:
                raise Exception(f"Unable to properly parse seeds map: {l}")

            source_start = parsed_line[1]
            destination_start = parsed_line[0]
            map_range = range(source_start, source_start + parsed_line[2] - 1)
            range_offset = destination_start - source_start

            maps_data[map_destination].add_range(r=map_range, offset=range_offset)

    return seeds, maps_data


def transform_seeds_to_location(
    seeds: List[int], soil_maps: Dict[str, TransformMap]
) -> int:
    # to_soils = np.vectorize(soil_maps["soil"])
    # to_fertilizer = np.vectorize(soil_maps["fertilizer"])
    # to_water = np.vectorize(soil_maps["water"])
    # to_light = np.vectorize(soil_maps["light"])
    # to_temperature = np.vectorize(soil_maps["temperature"])
    # to_humidity = np.vectorize(soil_maps["humidity"])
    # to_location = np.vectorize(soil_maps["location"])

    to_soils = soil_maps["soil"]
    to_fertilizer = soil_maps["fertilizer"]
    to_water = soil_maps["water"]
    to_light = soil_maps["light"]
    to_temperature = soil_maps["temperature"]
    to_humidity = soil_maps["humidity"]
    to_location = soil_maps["location"]

    return min(
        to_location(
            to_humidity(
                to_temperature(to_light(to_water(to_fertilizer(to_soils(seeds)))))
            )
        )
    )

    # soils = to_soils(seeds)
    # fertilizers = to_fertilizer(soils)
    # waters = to_water(fertilizers)
    # lights = to_light(waters)
    # temperatures = to_temperature(lights)
    # humidities = to_humidity(temperatures)
    # return np.min(to_location(humidities))


# def transform_to_location_wrapper(
#     seeds_queue: multiprocessing.JoinableQueue,
#     location_queue: multiprocessing.JoinableQueue,
#     soil_maps: Dict[str, TransformMap],
#     logger: logging.Logger = logging.getLogger(),
# ) -> None:
#     seed_chunks = 1
#     while True:
#         try:
#             seeds = seeds_queue.get()
#             min_location = transform_seeds_to_location(seeds=seeds, soil_maps=soil_maps)
#             location_queue.put(min_location)
#             seeds_queue.task_done()
#             logger.debug(
#                 f"Thread_Chunk_ID: {seed_chunks}. Min_location: {min_location}"
#             )
#             seed_chunks += 1
#         except queue.Queue.empty:
#             break


def parse_puzzle1(input: str) -> int:
    seeds, soil_maps = parse_input(input=input)

    location = transform_seeds_to_location(seeds=seeds, soil_maps=soil_maps)

    return location


# # def prepare_workers(
# #     seeds_queue: multiprocessing.JoinableQueue,
# #     location_queue: multiprocessing.JoinableQueue,
# #     soil_maps: Dict[str, TransformMap],
# #     workers_count: int = 6,
# #     logger: logging.Logger = logging.getLogger(),
# # ) -> List[multiprocessing.Process]:
# #     processes = []

# #     # creating processes
# #     for w in range(workers_count):
# #         p = multiprocessing.Process(
# #             target=transform_to_location_wrapper,
# #             args=(seeds_queue, location_queue, soil_maps, logger),
# #         )
# #         processes.append(p)
# #         p.start()

#     # # completing process
#     # for p in processes:
#     #     p.join()

#     # # print the output
#     # while not location_queue.empty():
#     #     print(location_queue.get())

#     return processes


# def parse_puzzle2(input: str) -> int:
#     seed_chunk_size = 2
#     workers_count = 8
#     seeds, soil_maps = parse_input(input=input)
#     min_location: Optional[int] = None
#     log = logging.getLogger()

#     seeds_queue = multiprocessing.JoinableQueue()
#     location_queue = multiprocessing.JoinableQueue()

#     workers = prepare_workers(
#         seeds_queue=seeds_queue,
#         location_queue=location_queue,
#         soil_maps=soil_maps,
#         workers_count=workers_count,
#         logger=log,
#     )

#     seed_chunks = 0
#     for seed_ranges in np.arange(0, int(len(seeds) / 2)):
#         seed_range_start = seeds[seed_ranges * 2]
#         seed_range_end = seeds[seed_ranges * 2] + seeds[seed_ranges * 2 + 1]

#         #     seeds_range = np.arange(seed_range_start, seed_range_end)
#         #     seeds_queue.put(seeds_range)
#         #     # transform_tasks.append(
#         #     #     asyncio.to_thread(transform_seeds_to_location, seeds_range, soil_maps)
#         #     # )

#         # for w in workers:
#         #     w.join()

#         seed_chunk_range_start = seed_range_start
#         while seed_chunk_range_start < seed_range_end:
#             seed_chunk_range_end = seed_chunk_range_start + seed_chunk_size
#             if seed_chunk_range_end > seed_range_end:
#                 seed_chunk_range_end = seed_range_end

#             seeds_range = np.arange(seed_chunk_range_start, seed_chunk_range_end)

#             seeds_queue.put(seeds_range)
#             seed_chunks += 1
#             # current_min_location = transform_seeds_to_location(
#             #     seeds=seeds_range, soil_maps=soil_maps
#             # )

#             # if min_location is None or min_location > current_min_location:
#             #     min_location = current_min_location

#             seed_chunk_range_start = seed_chunk_range_end + 1

#     log.debug(f"Generated seed chunks total: {seed_chunks}")

#     seeds_queue.join()

#     for w in workers:
#         w.terminate()

#     while not location_queue.empty():
#         location = location_queue.get()
#         if min_location is None or min_location > location:
#             min_location = location
#     #     print(location_queue.get())

#     # min_locations = await asyncio.gather(*transform_tasks)
#     # results = await asyncio.gather(*min_locations)

#     return min_location


def parse_puzzle2(input: str) -> int:
    seed_chunk_size = 2
    workers_count = 8
    seeds, soil_maps = parse_input(input=input)
    min_location: Optional[int] = None
    log = logging.getLogger()

    seeds_range = TransformRange(source="seeds")
    for seed_ranges in range(0, int(len(seeds) / 2)):
        seed_range_start = seeds[seed_ranges * 2]
        seed_range_end = seeds[seed_ranges * 2] + seeds[seed_ranges * 2 + 1]
        seeds_range.ranges.append(range(seed_range_start, seed_range_end))
    transformed = (
        seeds_range.transform(soil_maps["soil"])
        .transform(soil_maps["fertilizer"])
        .transform(soil_maps["water"])
        .transform(soil_maps["light"])
        .transform(soil_maps["temperature"])
        .transform(soil_maps["humidity"])
        .transform(soil_maps["location"])
    )

    return transformed.min_range_value()
