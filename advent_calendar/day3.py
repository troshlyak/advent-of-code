import re
from typing import Any, Callable, List, Optional

import numpy as np
from pydantic import BaseModel

import utils


def symbol_unmask(x: Any) -> Any:
    if x not in "0123456789.":
        return 1

    return 0


def gear_unmask(x: Any) -> Any:
    if x == "*":
        return 1

    return 0


def number_unmask(x: Any) -> Any:
    if x in "0123456789":
        return 1

    return 0


class SchemaNumber(BaseModel, arbitrary_types_allowed=True):
    x: int
    y: int
    value: int
    matrix_data: Optional[np.matrix] = None

    @property
    def length(self) -> int:
        return len(str(self.value))

    def apply_mask_to_matrix(self, func: Callable) -> bool:
        mask_func = np.vectorize(func)
        number_matrix_masked = mask_func(self.matrix_data)

        if np.sum(number_matrix_masked):
            return True

        return False

    @property
    def is_part(self) -> bool:
        return self.apply_mask_to_matrix(func=symbol_unmask)

    @property
    def is_gear_attached(self) -> bool:
        return self.apply_mask_to_matrix(func=gear_unmask)

    def is_number_attached(self) -> bool:
        return self.apply_mask_to_matrix(func=gear_unmask)

    @property
    def matrix_start_col(self) -> int:
        start_col = self.x - 1

        if start_col < 0:
            return 0
        else:
            return start_col

    @property
    def matrix_start_row(self) -> int:
        start_row = self.y - 1

        if start_row < 0:
            return 0
        else:
            return start_row

    @property
    def matrix_end_col(self) -> int:
        return self.x + self.length + 1

    @property
    def matrix_end_row(self) -> int:
        return self.y + 2

    def populate_matrix(self, engine_matrix: np.matrix) -> None:
        self.matrix_data = engine_matrix[
            self.matrix_start_row : self.matrix_end_row,
            self.matrix_start_col : self.matrix_end_col,
        ]
        self.validate_matrix()

    def validate_matrix(self) -> None:
        if self.matrix_data is not None:
            assert self.matrix_data.shape[0] in [2, 3]
            assert self.matrix_data.shape[1] in [self.length + 1, self.length + 2]

    def is_intersecting(self, number: "SchemaNumber") -> bool:
        self_rows = set(range(self.matrix_start_row, self.matrix_end_row))
        self_cols = set(range(self.matrix_start_col, self.matrix_end_col))
        num_rows = {number.y}
        num_cols = set(range(number.x, number.x + number.length))

        if self_rows.intersection(num_rows) and self_cols.intersection(num_cols):
            return True

        return False


class SchemaCog(SchemaNumber):
    attached_numbers: List[SchemaNumber] = []

    @property
    def is_gear(self) -> bool:
        if len(self.attached_numbers) == 2:
            return True

        return False

    @property
    def gear_value(self) -> int:
        if self.is_gear:
            return self.attached_numbers[0].value * self.attached_numbers[1].value

        return 0


def parse_schema_line(line: str, line_id: int, regex: str) -> list[SchemaNumber]:
    schema_numbers: List[SchemaNumber] = []

    number_regex = regex
    number_result = re.findall(number_regex, line.strip())

    line_with_stripped_numbers = line[:]
    for n in number_result:
        x = int(line_with_stripped_numbers.index(n))
        value = int(n) if n.isdigit() else 0
        schema_number = SchemaNumber(x=x, y=line_id, value=value)
        schema_numbers.append(schema_number)

        # Ensure we can handle duplicate numbers
        delete_number_str = "." * len(n)
        line_with_stripped_numbers = line_with_stripped_numbers.replace(
            n, delete_number_str, 1
        )

    return schema_numbers


def parse_schema_line_for_numbers(line: str, line_id: int) -> list[SchemaNumber]:
    return parse_schema_line(line=line, line_id=line_id, regex=r"[0-9]+")


def parse_schema_line_for_cogs(line: str, line_id: int) -> list[SchemaCog]:
    schema_items = parse_schema_line(line=line, line_id=line_id, regex=r"[\*]+")
    return [SchemaCog(**n.model_dump()) for n in schema_items]


def data_matrix(input: str) -> np.matrix:
    schema_lines = utils.multiline_str_to_list(
        multiline_str=input,
    )

    schema_array = [list(l) for l in schema_lines]
    return np.matrix(data=schema_array)


def get_schema_numbers(input: str) -> List[SchemaNumber]:
    schema_part_numbers: List[SchemaNumber] = []

    schema_lines = utils.multiline_str_to_list(
        multiline_str=input,
    )

    for index, l in enumerate(schema_lines):
        schema_part_numbers.extend(parse_schema_line_for_numbers(line=l, line_id=index))

    schema_matrix = data_matrix(input=input)
    for p in schema_part_numbers:
        p.populate_matrix(engine_matrix=schema_matrix)
    return schema_part_numbers


def get_schema_cogs(input: str) -> List[SchemaCog]:
    schema_cogs: List[SchemaCog] = []

    schema_lines = utils.multiline_str_to_list(
        multiline_str=input,
    )

    for index, l in enumerate(schema_lines):
        schema_cogs.extend(parse_schema_line_for_cogs(line=l, line_id=index))

    schema_matrix = data_matrix(input=input)
    for p in schema_cogs:
        p.populate_matrix(engine_matrix=schema_matrix)
    return schema_cogs


def parse_puzzle1(input: str) -> int:
    schema_part_numbers = get_schema_numbers(input=input)

    engine_parts = [n for n in schema_part_numbers if n.is_part]
    engine_parts_values = [n.value for n in engine_parts]

    return sum(engine_parts_values)


def parse_puzzle2(input: str) -> int:
    schema_part_numbers = get_schema_numbers(input=input)
    numbers_with_gears = [n for n in schema_part_numbers if n.is_gear_attached]

    schema_cogs: List[SchemaCog] = get_schema_cogs(input=input)
    cogs_with_numbers = [c for c in schema_cogs if c.is_number_attached]

    for c in cogs_with_numbers:
        for n in numbers_with_gears:
            if c.is_intersecting(number=n):
                c.attached_numbers.append(n)

    schema_gears = [g for g in cogs_with_numbers if g.is_gear]

    schema_gear_values = [c.gear_value for c in schema_gears]

    return sum(schema_gear_values)
