import logging
import multiprocessing
import queue
import re
from typing import Dict, List, Optional

from pydantic import BaseModel

import utils


class SeedMapEntry(BaseModel):
    destination_start: int
    source_start: int
    length: int


class SeedMap(BaseModel):
    source: str
    destination: str
    entries: List[SeedMapEntry] = []


class TransformMap(BaseModel, arbitrary_types_allowed=True):
    source: str
    destination: str
    ranges: List[range] = []
    range_offset: List[int] = []

    def add_range(self, r: range, offset: int) -> None:
        self.ranges.append(r)
        self.range_offset.append(offset)

    def __call__(self, transform: int) -> int:
        for index, r in enumerate(self.ranges):
            if transform >= r.start and transform <= r.stop:
                return transform + self.range_offset[index]

        return transform


class TransformRange(BaseModel, arbitrary_types_allowed=True):
    source: str
    ranges: List[range] = []

    def transform(self, map: TransformMap) -> "TransformRange":
        range_points: List[List[int]] = []
        transformed_range = TransformRange(source=map.destination)
        for index, sr in enumerate(self.ranges):
            range_points.append([sr.start, sr.stop])
            for tr in map.ranges:
                if tr.start > sr.start and tr.start < sr.stop:
                    # if tr.start not in range_points[index]:
                    #     range_points[index].extend([tr.start, tr.start])
                    # new_start = tr.start
                    # if new_start not in range_points[index]:
                    range_points[index].append(tr.start)
                    addon_point = tr.start - 1
                    # if new_stop not in range_points[index]:
                    #     range_points[index].append(new_stop)
                if tr.stop > sr.start and tr.stop < sr.stop:
                    # if tr.stop not in range_points[index]:
                    #     range_points[index].extend([tr.stop, tr.stop])
                    # new_start = new_stop + 1
                    # if new_start not in range_points[index]:
                    #     range_points[index].append(new_start)
                    # new_stop = tr.stop
                    # if new_stop not in range_points[index]:
                    addon_point = tr.stop + 1
                    range_points[index].append(tr.stop)

            if len(range_points[index]) % 2:
                range_points[index].append(addon_point)

        for points in range_points:
            reversed = False
            if points[0] > points[1]:
                reversed = True
            points.sort(reverse=reversed)
            for i in range(0, int(len(points) / 2)):
                r_start = map(points[i * 2])
                r_stop = map(points[i * 2 + 1])
                # if r_start <= r_stop:
                new_range = range(r_start, r_stop)
                # else:
                #     new_range = range(r_stop, r_start)
                transformed_range.ranges.append(new_range)

        return transformed_range

    def min_range_value(self) -> int:
        min_value: Optional[int] = None
        for r in self.ranges:
            if min_value is None or min_value > r.start:
                min_value = r.start
            if min_value is None or min_value > r.stop:
                min_value = r.stop

        return min_value


def parse_input(input: str) -> (List[int], Dict[str, TransformMap]):
    input_lines = utils.multiline_str_to_list(multiline_str=input)
    line_regex = r"^([\w-]+):?\s+(map)?([\d\s]+)?"

    maps_data: Dict[str, TransformMap] = {}
    seeds: List[int] = []
    map_name: Optional[str] = None
    map_source: Optional[str] = None
    map_destination: Optional[str] = None
    for l in input_lines:
        results = re.findall(line_regex, l)
        regex_matches = results[0]

        if map_name is None:
            map_name = regex_matches[0]
            seeds = [int(s) for s in re.split("\s+", regex_matches[2])]
            continue

        if regex_matches[1] == "map":
            map_name = regex_matches[0]
            map_source, _, map_destination = map_name.split("-")
            maps_data[map_destination] = TransformMap(
                source=map_source, destination=map_destination
            )
            continue
        else:
            parsed_line = [int(i) for i in re.split("\s+", l)]
            if len(parsed_line) != 3:
                raise Exception(f"Unable to properly parse seeds map: {l}")

            source_start = parsed_line[1]
            destination_start = parsed_line[0]
            map_range = range(source_start, source_start + parsed_line[2] - 1)
            range_offset = destination_start - source_start

            maps_data[map_destination].add_range(r=map_range, offset=range_offset)

    return seeds, maps_data


def transform_seed_to_location(seed: int, soil_maps: Dict[str, TransformMap]) -> int:
    to_soils = soil_maps["soil"]
    to_fertilizer = soil_maps["fertilizer"]
    to_water = soil_maps["water"]
    to_light = soil_maps["light"]
    to_temperature = soil_maps["temperature"]
    to_humidity = soil_maps["humidity"]
    to_location = soil_maps["location"]

    locations = []

    return to_location(
        to_humidity(to_temperature(to_light(to_water(to_fertilizer(to_soils(seed))))))
    )


def transform_seeds_to_location(
    seeds: List[int], soil_maps: Dict[str, TransformMap]
) -> int:
    locations = [transform_seed_to_location(seed=s, soil_maps=soil_maps) for s in seeds]

    return min(locations)


def parse_puzzle1(input: str) -> int:
    seeds, soil_maps = parse_input(input=input)

    location = transform_seeds_to_location(seeds=seeds, soil_maps=soil_maps)

    return location


def parse_puzzle2(input: str) -> int:
    seed_chunk_size = 2
    workers_count = 8
    seeds, soil_maps = parse_input(input=input)
    min_location: Optional[int] = None
    log = logging.getLogger()

    seeds_range = TransformRange(source="seeds")
    for seed_ranges in range(0, int(len(seeds) / 2)):
        seed_range_start = seeds[seed_ranges * 2]
        seed_range_end = seeds[seed_ranges * 2] + seeds[seed_ranges * 2 + 1] - 1
        seeds_range.ranges.append(range(seed_range_start, seed_range_end))
    transformed = (
        seeds_range.transform(soil_maps["soil"])
        .transform(soil_maps["fertilizer"])
        .transform(soil_maps["water"])
        .transform(soil_maps["light"])
        .transform(soil_maps["temperature"])
        .transform(soil_maps["humidity"])
        .transform(soil_maps["location"])
    )

    return transformed.min_range_value()
