import re
from typing import List

from pydantic import BaseModel

import utils


class ElfCard(BaseModel):
    id: int
    games: List[int]
    winning_games: List[int]

    @property
    def won_games(self) -> List[int]:
        games_set = set(self.games)

        if len(games_set) != len(self.games):
            raise Exception(f"Duplicate game value detected in games: {self.games}")

        winning_set = set(self.winning_games)

        return games_set.intersection(winning_set)

    @property
    def value(self) -> None:
        if self.won_games:
            return 1 * (2 ** (len(self.won_games) - 1))

        return 0

    @staticmethod
    def parse_line_to_card(line: str) -> "ElfCard":
        game_regex = r"^Card\s+(\d+):\s+(.*)"
        game_pattern = re.compile(game_regex)
        regex_groups = game_pattern.findall(line)

        if regex_groups and len(regex_groups) < 1:
            raise Exception(f"Unable to parse card: {line}")

        game_data = regex_groups[0][1]
        card_games_data, winning_games_data = game_data.split("|")

        elf_card_games = list(map(int, re.split("\s+", card_games_data.strip())))
        winning_card_games = list(map(int, re.split("\s+", winning_games_data.strip())))
        elf_card = ElfCard(
            id=int(regex_groups[0][0]),
            games=elf_card_games,
            winning_games=winning_card_games,
        )

        return elf_card


def get_elf_cards(input) -> List[ElfCard]:
    elf_cards: List[ElfCard] = []
    schema_lines = utils.multiline_str_to_list(
        multiline_str=input,
    )

    for l in schema_lines:
        elf_cards.append(ElfCard.parse_line_to_card(line=l))
    return elf_cards


def parse_puzzle1(input: str) -> int:
    elf_cards = get_elf_cards(input=input)

    elf_card_values = [c.value for c in elf_cards]

    return sum(elf_card_values)


def parse_puzzle2(input: str) -> int:
    elf_cards = get_elf_cards(input=input)

    index = 0
    while len(elf_cards) > index:
        current_card = elf_cards[index]
        cards_to_add_count = len(current_card.won_games)
        cards_to_add = elf_cards[current_card.id : current_card.id + cards_to_add_count]
        elf_cards.extend(cards_to_add)
        index += 1

    return len(elf_cards)
