from typing import List, Optional

import utils


def get_calibration_value(line: str) -> Optional[int]:
    decimal_digit = utils.get_first_digit(line=line)
    single_digit = utils.get_first_digit(line=line[::-1])

    if decimal_digit and single_digit:
        return decimal_digit * 10 + single_digit

    return None


def calibrate_values(input: str) -> List[int]:
    calibration_lines = utils.multiline_str_to_list(multiline_str=input)
    calibration_values: List[int] = []

    for l in calibration_lines:
        v = get_calibration_value(line=l)
        if v:
            calibration_values.append(v)
        else:
            raise Exception(
                f"Unable to calculate calibration for line: {l}. Not enough numbers detected"
            )

    return calibration_values
