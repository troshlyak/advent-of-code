import re
from typing import List

from pydantic import BaseModel

import utils


class ElfBag(BaseModel):
    red: int = 0
    blue: int = 0
    green: int = 0

    def __ge__(self, o: "ElfBag") -> bool:
        if self.red >= o.red and self.blue >= o.blue and self.green >= o.green:
            return True

        return False

    def __le__(self, o: "ElfBag") -> bool:
        if self.red <= o.red and self.blue <= o.blue and self.green <= o.green:
            return True

        return False

    def power(self) -> int:
        return self.red * self.green * self.blue


class ElfGame(BaseModel):
    id: int
    elf_hands: List[ElfBag] = []


def parse_game_input_to_model(line: str) -> ElfGame:
    game_regex = r"^Game\s+(\d+):\s+(.*)"
    game_pattern = re.compile(game_regex)
    regex_groups = game_pattern.findall(line)

    if regex_groups and len(regex_groups) < 1:
        raise Exception(f"Unable to parse game: {line}")

    elf_game = ElfGame(id=int(regex_groups[0][0]))
    elf_game.elf_hands = parse_game_variants(line=regex_groups[0][1])

    return elf_game


def parse_game_variants(line: str) -> List[ElfBag]:
    game_variants_data = line.split(";")
    game_variants: List[ElfBag] = []

    for g in game_variants_data:
        game_variants.append(parse_game_variant(g))

    return game_variants


def parse_game_variant(line: str) -> ElfBag:
    elf_bag = ElfBag()

    for cubes_data in line.split(","):
        cube_regex = r"^(\d+)\s+(blue|red|green)"
        cube_pattern = re.compile(cube_regex)
        cube_result = cube_pattern.findall(cubes_data.strip())

        if len(cube_result) < 1:
            raise Exception(f"Unable to parse cube data: {cubes_data}")

        cube_count = cube_result[0][0]
        cube_colour = cube_result[0][1]
        if not getattr(elf_bag, cube_colour):
            setattr(elf_bag, cube_colour, int(cube_count))
        else:
            raise Exception(
                f"Cannot set count for cube color {cube_colour} - they are already set to {getattr(elf_bag, cube_colour)}"
            )

    return elf_bag


def is_game_possible(game: ElfGame, bag: ElfBag) -> bool:
    possible_variants = [
        is_bag_possible(drawn_bag=v, actual_bag=bag) for v in game.elf_hands
    ]

    return all(possible_variants)


def is_bag_possible(drawn_bag: ElfBag, actual_bag: ElfBag) -> bool:
    return actual_bag >= drawn_bag


def parse_puzzle1(input: str, bag: ElfBag) -> int:
    possible_games: List[ElfGame] = []
    games_input = utils.multiline_str_to_list(
        multiline_str=input,
    )

    games = [parse_game_input_to_model(l) for l in games_input]

    for g in games:
        if is_game_possible(game=g, bag=bag):
            possible_games.append(g)

    return sum([g.id for g in possible_games])


def lowest_possible_bag(game: ElfGame) -> ElfBag:
    lowest_bag = ElfBag()

    for v in game.elf_hands:
        if lowest_bag.red < v.red:
            lowest_bag.red = v.red
        if lowest_bag.green < v.green:
            lowest_bag.green = v.green
        if lowest_bag.blue < v.blue:
            lowest_bag.blue = v.blue

    return lowest_bag


def parse_puzzle2(input: str) -> int:
    games_input = utils.multiline_str_to_list(
        multiline_str=input,
    )

    games = [parse_game_input_to_model(l) for l in games_input]
    lowest_possible_bags = [lowest_possible_bag(game=g) for g in games]

    return sum([b.power() for b in lowest_possible_bags])
