from typing import List, Optional


def multiline_str_to_list(multiline_str: str) -> List[str]:
    return [s.strip() for s in multiline_str.split("\n") if s]


def word_digit(line: str) -> Optional[int]:
    digit_words: List[str] = [
        "zero",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine",
    ]

    for d in digit_words:
        if line.startswith(d) or line.startswith(d[::-1]):
            return digit_words.index(d)

    return None


def get_first_digit(line: str) -> Optional[int]:
    characters = list(line)
    for index, c in enumerate(characters):
        word_digit_value = word_digit(line[index:])
        if c.isdigit():
            return int(c)
        if word_digit_value:
            return word_digit_value

    return None
