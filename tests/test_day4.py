from typing import List

import pytest

from advent_calendar import day4


@pytest.fixture
def puzzle1_input() -> str:
    return """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""


@pytest.fixture
def puzzle2_input() -> str:
    return """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""


@pytest.fixture
def puzzle1_output() -> int:
    return 13


@pytest.fixture
def puzzle2_output() -> int:
    return 30


def test_puzzle1(puzzle1_input: str, puzzle1_output: int) -> None:
    puzzle_result: int = day4.parse_puzzle1(input=puzzle1_input)
    assert puzzle_result == puzzle1_output


def test_puzzle2(puzzle2_input: str, puzzle2_output: int) -> None:
    puzzle_result: int = day4.parse_puzzle2(input=puzzle2_input)
    assert puzzle_result == puzzle2_output
