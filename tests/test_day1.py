from typing import List

import pytest

from advent_calendar import day1


@pytest.fixture
def calibration_values_input_str() -> str:
    return """two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen"""


@pytest.fixture
def calibration_values_input_list() -> List[str]:
    return [
        "two1nine",
        "eightwothree",
        "abcone2threexyz",
        "xtwone3four",
        "4nineeightseven2",
        "zoneight234",
        "7pqrstsixteen",
    ]


def test_puzzle1(calibration_values_input_str: str) -> None:
    output = 281

    calibration_values: List[int] = day1.calibrate_values(
        input=calibration_values_input_str
    )
    assert sum(calibration_values) == output
