from typing import List

import pytest

from advent_calendar import day5


@pytest.fixture
def puzzle1_input() -> str:
    return """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""


@pytest.fixture
def puzzle2_input() -> str:
    return """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""


@pytest.fixture
def puzzle1_output() -> int:
    return 35


@pytest.fixture
def puzzle2_output() -> int:
    return 46


# @pytest.mark.asyncio
def test_puzzle1(puzzle1_input: str, puzzle1_output: int) -> None:
    puzzle_result: int = day5.parse_puzzle1(input=puzzle1_input)
    assert puzzle_result == puzzle1_output


# @pytest.mark.asyncio
def test_puzzle2(puzzle2_input: str, puzzle2_output: int) -> None:
    puzzle_result: int = day5.parse_puzzle2(input=puzzle2_input)
    assert puzzle_result == puzzle2_output
