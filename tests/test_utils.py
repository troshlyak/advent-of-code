from typing import List

import pytest

import utils


@pytest.fixture
def multiline_str() -> str:
    return """two1nine
            eightwothree
            abcone2threexyz
            xtwone3four
            4nineeightseven2
            zoneight234
            7pqrstsixteen"""


@pytest.fixture
def multiline_list() -> List[str]:
    return [
        "two1nine",
        "eightwothree",
        "abcone2threexyz",
        "xtwone3four",
        "4nineeightseven2",
        "zoneight234",
        "7pqrstsixteen",
    ]


def test_multiline_str_to_list(multiline_str: str, multiline_list: List[str]) -> None:
    test_str_list = utils.multiline_str_to_list(multiline_str=multiline_str)
    assert test_str_list == multiline_list
