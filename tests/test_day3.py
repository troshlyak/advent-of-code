from typing import List

import pytest

from advent_calendar import day3


@pytest.fixture
def puzzle1_input() -> str:
    return """467.467..1
            ..*......$
            ..35..633.
            ......#...
            617*......
            .....+.58.
            ..592.....
            ......755.
            ...$.*....
            .664.598.."""


@pytest.fixture
def puzzle2_input() -> str:
    return """467..114..
            ...*......
            ..35..633.
            ......#...
            617*......
            .....+.58.
            ..592.....
            ......755.
            ...$.*....
            .664.598.."""


@pytest.fixture
def puzzle1_output() -> int:
    return 4362
    # return 4361


@pytest.fixture
def puzzle2_output() -> int:
    return 467835


def test_puzzle1(puzzle1_input: str, puzzle1_output: int) -> None:
    puzzle_result: int = day3.parse_puzzle1(input=puzzle1_input)
    assert puzzle_result == puzzle1_output


def test_puzzle2(puzzle2_input: str, puzzle2_output: int) -> None:
    puzzle_result: int = day3.parse_puzzle2(input=puzzle2_input)
    assert puzzle_result == puzzle2_output
