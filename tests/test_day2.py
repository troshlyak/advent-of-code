from typing import List

import pytest

from advent_calendar import day2


@pytest.fixture
def puzzle1_input() -> str:
    return """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""


@pytest.fixture
def puzzle2_input() -> str:
    return """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
            Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
            Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
            Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
            Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""


@pytest.fixture
def puzzle1_output() -> int:
    return 8


@pytest.fixture
def puzzle2_output() -> int:
    return 2286


@pytest.fixture
def elf_bag() -> day2.ElfBag:
    return day2.ElfBag(red=12, green=13, blue=14)


def test_puzzle1(puzzle1_input: str, elf_bag: day2.ElfBag, puzzle1_output: int) -> None:
    puzzle_result: int = day2.parse_puzzle1(input=puzzle1_input, bag=elf_bag)
    assert puzzle_result == puzzle1_output


def test_puzzle2(puzzle2_input: str, puzzle2_output: int) -> None:
    puzzle_result: int = day2.parse_puzzle2(input=puzzle2_input)
    assert puzzle_result == puzzle2_output
